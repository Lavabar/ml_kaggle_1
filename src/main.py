def sum_a_b(a: int, b: int):
    """
    Summing parameters
    :param a:
    :param b:
    :return:
    """

    return a + b


def main():
    print("Hello world!")
    print(sum_a_b(1, 3))


if __name__ == "__main__":
    main()
